FROM golang:alpine

RUN apk --no-cache --update add tzdata ca-certificates git && update-ca-certificates

ENV GOFLAGS -mod=vendor
ENV CGO_ENABLED=0

WORKDIR /opt/app
ADD . .
RUN go mod vendor
RUN go build -o /app


FROM scratch
COPY --from=0 /app /app
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /usr/share/zoneinfo /usr/share/zoneinfo

EXPOSE [8080]

ENTRYPOINT ["/app"]