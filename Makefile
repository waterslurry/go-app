GOFLAGS := -mod=vendor
APP_COMMIT := $(shell git log -1 --format="%h")-$(shell git diff-files --quiet || echo "dirty")
APP_DATE := $(shell date +"%Y-%m-%dT%H-%M-%S")
APP_VERSION := $(shell git describe --exact-match --tags $(git log -n1 --pretty='%h') 2> /dev/null || git rev-parse --abbrev-ref HEAD)

.DEFAULT_GOAL := help
.PHONY: help
help:  ## Show help
	@awk 'BEGIN {FS = ":.*?## "} /^[\/a-zA-Z_-]+:.*?## / {sub("\\\\n",sprintf("\n%22c"," "), $$2);printf "\033[36m%-25s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.env:
	@cp .env.dist .env

.PHONY: version
version:  ## Show app version (when running locally)
	@echo ${APP_VERSION}-${APP_COMMIT}-${APP_DATE}

.PHONY: test
test: .env ## Run tests
	@go test .

.PHONY: test-race
test-race: .env ## Run more extensive tests
	@CGO_ENABLED=1 go test -race ./...

.PHONY: cover
cover:  ## Run tests and generate coverage report ("coverage.txt")
	@go test -coverpkg=./... -coverprofile=coverage.txt ./...

.PHONY: build
build:  ## Build binary
	@go build -ldflags="-X main.version=${APP_VERSION}-${APP_COMMIT}-${APP_DATE}" .

.PHONY: vendor
vendor:  ## Get vendor dependencies
	@go mod vendor

.PHONY: vet
vet:  ## Static analysis with go vet
	@go vet $(go list ./... | grep -v vendor)

.PHONY: fmt
fmt:  ## Fix formatting
	@go fmt $(go list ./... | grep -v vendor)

.PHONY: lint
lint:  ## Static analysis with golint
	@golint $(go list ./... | grep -v vendor)

.PHONY: imports
imports:  ## Fix imports with go imports
	@which goimports > /dev/null || go get golang.org/x/tools/cmd/goimports 2> /dev/null
	@find . -type f -name '*.go' -not -path "./vendor/*" | xargs goimports -w

.PHONY: tidy
tidy:  ## Cleanup modules
	@go mod tidy

.PHONY: run
run: .env ## Run app
	@#no reason to use semver here
	@env $(shell cat .env | grep -ve "^#" | xargs) go run -ldflags="-X main.version=${APP_VERSION}-${APP_COMMIT}-${APP_DATE}" .

.PHONY: docker
docker:  ## Build docker image
	@docker build -t goapp .
