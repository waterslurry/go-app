module goapp

go 1.14

require (
	github.com/gorilla/mux v1.8.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/mailgun/mailgun-go/v4 v4.1.4
)
