package main

import (
	"fmt"
	"goapp/cmd"
)

var version = "unspecified"

func main() {
	fmt.Printf("Version: %s\r\n", version)
	cmd.Execute()
}
