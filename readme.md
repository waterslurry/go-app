#  🌊 Water Slurry

## Maintainers
- Main
  - [danielstokes](https://github.com/danielstokes)
  
## Idea
Water is so abundant, but clean water is not, depending on where you are.
An api that allows us to query fresh water locations.  In the case of fresh water lakes and rivers, mark only the natural spring location.

## Product Spec
- Rest api to manage water source location, flow rate and name
- Block robots [wiki](https://en.wikipedia.org/wiki/Robots_exclusion_standard)
- Support CORS [spec](https://fetch.spec.whatwg.org/)

## Environments
- Dev only, maintained with terraform [here](https://gitlab.com/waterslurry/infra)

## Deployment
- Deployed to heroku via git remote and gitlab-ci pipeline

## Technology stack

- **Language** - [Go](https://golang.org/)
- **Dependency Management** - [go modules](https://github.com/golang/go/wiki/Modules)

### Configuration
All configuration is done through environment variables currently.  The data types are native to golang.  Look at the docs for more info.

## Environment variables

| Name | Type | Default | Required
|---|---|---|---
| [`DEBUG`](#debug) | `bool` | `false` | 

##### `DEBUG`
Enable debug mode.  Shows more logging info.


## Commands

| command           | description                                                  
|-------------------|---
| `make`            | Show help
| `make help`       | Show help
| `make version`    | Show version app would use
| `make test`       | Run tests
| `make test-race`  | Run tests with race detector active
| `make cover`      | Run tests and build coverage report
| `make build`      | Build the app
| `make vendor`     | Pull in vendor dependencies
| `make vet`        | Run static Analysis with `go vet`
| `make fmt`        | Run coding formatting
| `make lint`       | Run static analysis with `go lint`
| `make imports`    | Cleanup imports
| `make tidy`       | Cleanup modules
| `make run`        | Run the app
| `make docker`     | Build docker image
