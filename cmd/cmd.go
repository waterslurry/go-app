package cmd

import (
	"fmt"
	"goapp/internal/config"
	"goapp/internal/echocon"
	"goapp/internal/service/events"
	"goapp/internal/service/mail"
	"goapp/internal/watercon"
	"goapp/internal/waterrepo"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func Execute() {

	r := mux.NewRouter()

	waterrep := waterrepo.NewRepoMemory()

	conf := config.New()

	m := mail.New(conf.MailgunDomain, conf.MailgunApiKey)
	e := events.New(&m, conf.MailgunTargetEmail, conf.MailgunTargetEmail)
	e.Start()
	defer e.Stop()

	echocon.New(r)
	watercon.New(r, waterrep, e)

	serv := http.Server{
		Handler: PanicHandler(r),
		Addr:    ":" + strconv.Itoa(conf.Port),
	}
	err := serv.ListenAndServe()
	if err != http.ErrServerClosed {
		panic(err)
	}
}

func PanicHandler(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if rec := recover(); rec != nil {
				http.Error(w, fmt.Sprint(rec), http.StatusInternalServerError)
			}
		}()
		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}
