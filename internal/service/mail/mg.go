package mail

import (
	"context"
	"time"

	"github.com/mailgun/mailgun-go/v4"
)

type Mail struct {
	domain string
	apiKey string
}

func New(domain, apiKey string) Mail {
	return Mail{
		domain: domain,
		apiKey: apiKey,
	}
}

func (m Mail) Send(src, dst, subject, template string) {
	mg := mailgun.NewMailgun(m.domain, m.apiKey)
	msg := mg.NewMessage(src, subject, template, dst)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	_, _, err := mg.Send(ctx, msg)
	if err != nil {
		panic(err)
	}
}
