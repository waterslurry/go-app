package events

import (
	"fmt"
	"goapp/internal/service/mail"
	"sync"
)

type Events struct {
	started bool
	msgs    chan struct{}
	quit    chan struct{}
	lock    *sync.Mutex
	mail    *mail.Mail
	src     string
	dst     string
}

func New(mail *mail.Mail, src string, dst string) Events {
	return Events{
		msgs: make(chan struct{}),
		quit: make(chan struct{}),
		mail: mail,
		src:  src,
		dst:  dst,
		lock: &sync.Mutex{},
	}
}

func (e Events) Start() {
	e.lock.Lock()
	defer e.lock.Unlock()
	if e.started {
		return
	}
	e.started = true
	go func() {
		for {
			select {
			case <-e.quit:
				return
			case <-e.msgs:
				fmt.Println("sending message")
				e.mail.Send(e.src, e.dst, "new", "New")
			}
		}
	}()
}

func (e Events) Trigger() {
	e.msgs <- struct{}{}
}

func (e Events) Stop() {
	e.lock.Lock()
	defer e.lock.Unlock()
	if !e.started {
		return
	}
	e.started = false
	e.quit <- struct{}{}
}
