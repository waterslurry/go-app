package config

import "github.com/kelseyhightower/envconfig"

type Config struct {
	Debug              bool   `envconfig:"debug" default:"false"`
	MailgunApiKey      string `envconfig:"mailgun_api_key" required:"true"`
	MailgunDomain      string `envconfig:"mailgun_domain" default:"waterslurry.ml"`
	MailgunTargetEmail string `envconfig:"mailgun_target_email" required:"true"`
	Port               int    `envconfig:"port" default:"8080"`
}

func New() Config {
	var cfg Config
	err := envconfig.Process("", &cfg)
	if err != nil {
		panic(err)
	}

	return cfg
}
