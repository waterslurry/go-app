package waterrepo

import "errors"

type Water struct {
	Name                  string
	Latitude              float64
	Longitude             float64
	LitersFlowRatePerHour float64
}

type Repo interface {
	Count() (int, error)
	GetAll() ([]Water, error)
	GetByName(string) (*Water, error)
	Add(*Water) error
	Change(*Water) error
	Remove(string) error
}

var ErrWaterNotFound = errors.New("water not found")
var ErrWaterAlreadyFound = errors.New("water already found")

type RepoMemory struct {
	sources map[string]Water
}

func NewRepoMemory() *RepoMemory {
	return &RepoMemory{
		sources: map[string]Water{},
	}
}

func (r *RepoMemory) GetByName(name string) (*Water, error) {
	if water, ok := r.sources[name]; ok {
		return &water, nil
	}
	return nil, ErrWaterNotFound
}

func (r *RepoMemory) Add(water *Water) error {
	if _, ok := r.sources[water.Name]; ok {
		return ErrWaterAlreadyFound
	}
	r.sources[water.Name] = *water
	return nil
}

func (r *RepoMemory) Change(water *Water) error {
	if _, ok := r.sources[water.Name]; ok {
		r.sources[water.Name] = *water
		return nil
	}
	return ErrWaterNotFound
}
func (r *RepoMemory) Remove(name string) error {
	if _, ok := r.sources[name]; ok {
		delete(r.sources, name)
		return nil
	}
	return ErrWaterNotFound
}
func (r *RepoMemory) GetAll() ([]Water, error) {
	var sources []Water
	for _, water := range r.sources {
		sources = append(sources, water)
	}
	return sources, nil
}
func (r *RepoMemory) Count() (int, error) {
	return len(r.sources), nil
}
