package echocon

import (
	"net/http"

	"github.com/gorilla/mux"
)

type EchoController struct{}

func New(m *mux.Router) EchoController {
	c := EchoController{}
	m.HandleFunc("/hello2", c.HandleHello)
	return c
}

func (c EchoController) HandleHello(w http.ResponseWriter, r *http.Request) {
	_, err := w.Write([]byte("world2"))
	if err != nil {
		panic(err)
	}
}
