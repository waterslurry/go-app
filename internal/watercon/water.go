package watercon

import (
	"encoding/json"
	"goapp/internal/service/events"
	"goapp/internal/waterrepo"
	"net/http"

	"github.com/gorilla/mux"
)

type WaterController struct {
	repo   waterrepo.Repo
	events events.Events
}

func New(m *mux.Router, repo waterrepo.Repo, evts events.Events) WaterController {
	c := WaterController{
		repo:   repo,
		events: evts,
	}

	m.HandleFunc("/api/v1/water", c.HandleWaterList).Methods("GET")
	m.HandleFunc("/api/v1/water", c.HandleWaterCreate).Methods("POST")
	m.HandleFunc("/api/v1/water/{name}", c.HandleWaterFetch).Methods("GET")
	m.HandleFunc("/api/v1/water/{name}", c.HandleWaterDelete).Methods("DELETE")
	m.HandleFunc("/api/v1/water/{name}", c.HandleWaterUpdate).Methods("PATCH")
	return c
}

func (c WaterController) HandleWaterList(w http.ResponseWriter, req *http.Request) {
	sources, err := c.repo.GetAll()
	if err != nil {
		panic(err)
	}
	if len(sources) > 0 {
		err = json.NewEncoder(w).Encode(sources)
		if err != nil {
			panic(err)
		}
		return
	}
	_, err = w.Write([]byte("[]"))
	if err != nil {
		panic(err)
	}
}

func (c WaterController) HandleWaterCreate(w http.ResponseWriter, req *http.Request) {
	if req.Body == nil {
		http.Error(w, "invalid request", http.StatusBadRequest)
		return
	}
	ct := req.Header.Get("content-type")
	if ct != "application/json" {
		http.Error(w, "unsupported content-type", http.StatusUnsupportedMediaType)
		return
	}
	water := waterrepo.Water{}
	err := json.NewDecoder(req.Body).Decode(&water)
	if err != nil {
		http.Error(w, "unable to deserialize body", http.StatusNotAcceptable)
		return
	}
	if water.Name == "" {
		http.Error(w, "field name with at least 1 character is required", http.StatusNotAcceptable)
		return
	}
	err = c.repo.Add(&water)
	if err == waterrepo.ErrWaterAlreadyFound {
		http.Error(w, "water already found", http.StatusConflict)
		return
	} else if err != nil {
		panic(err)
	}
	c.events.Trigger()
}

func (c WaterController) HandleWaterFetch(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	name := vars["name"]
	water, err := c.repo.GetByName(name)
	if err == waterrepo.ErrWaterNotFound {
		http.Error(w, "water not found", http.StatusNotFound)
		return
	}
	err = json.NewEncoder(w).Encode(water)
	if err != nil {
		panic(err)
	}
}

func (c WaterController) HandleWaterUpdate(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	name := vars["name"]
	if req.Body == nil {
		http.Error(w, "invalid request", http.StatusBadRequest)
		return
	}
	ct := req.Header.Get("content-type")
	if ct != "application/json" {
		http.Error(w, "unsupported content-type", http.StatusUnsupportedMediaType)
		return
	}
	water := waterrepo.Water{}
	err := json.NewDecoder(req.Body).Decode(&water)
	if err != nil {
		http.Error(w, "nope", http.StatusNotAcceptable)
		return
	}
	if water.Name != "" {
		http.Error(w, "field name can't be specified in update request", http.StatusNotAcceptable)
		return
	}
	water.Name = name
	err = c.repo.Change(&water)
	if err == waterrepo.ErrWaterNotFound {
		http.Error(w, "water not found", http.StatusNotFound)
		return
	} else if err != nil {
		panic(err)
	}
}

func (c WaterController) HandleWaterDelete(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	name := vars["name"]
	err := c.repo.Remove(name)
	if err == waterrepo.ErrWaterNotFound {
		http.Error(w, "water not found", http.StatusNotFound)
		return
	} else if err != nil {
		panic(err)
	}
}
